from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("accounts/", include("accounts.urls")),
    path("tasks/", include("tasks.urls")),
    path(
        "",
        RedirectView.as_view(url=reverse_lazy("list_projects")),
        name="home",
    ),
]
